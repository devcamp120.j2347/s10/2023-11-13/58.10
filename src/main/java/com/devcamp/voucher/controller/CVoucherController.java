package com.devcamp.voucher.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucher.model.CVoucher;
import com.devcamp.voucher.repository.IVoucherRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CVoucherController {
    @Autowired
    private IVoucherRepository pVoucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        List<CVoucher> cVouchers = new ArrayList<CVoucher>();
        this.pVoucherRepository.findAll().forEach(cVouchers::add);

        return new ResponseEntity<List<CVoucher>>(cVouchers, HttpStatus.OK);
    }
}
